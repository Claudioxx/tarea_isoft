const assert = require('assert');
const multiply = require('../src/index'); 

describe('Multiplicación', function() {
  it('debería retornar 6 para 2 * 3', function() {
    assert.strictEqual(multiply(2, 3), 6);
  });

  it('debería retornar 10 para 2 * 5', function() {
    assert.strictEqual(multiply(2, 5), 10);
  });
});
